<?php

namespace App\Form;

use App\Entity\Course;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [ 'label' => 'Nom du cours'])
            ->add('smallDescription', TextareaType::class, [ 'label' => 'Courte Description'])
            ->add('duration', NumberType::class, [ 'label' => 'Durée de la formation'])
            ->add('price', NumberType::class, [ 'label' => 'Prix de la formation'])
            ->add('isPublished', ChoiceType::class, [
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Image du cours',
                'required' => false,
                'empty_data' => 'default.jpg',
                'attr' => [
                    'placeholder' => 'Image',
                    'class' => 'imgForm'
            ],
                'download_link' => false,
                'allow_delete' => false
            ])
            ->add('fullDescription', TextareaType::class, ['label' => 'Description complète'])
            ->add('category', EntityType::class, 
            [
                'class' => 'App:CourseCategory',
                'choice_label' => 'name'
                ])
            ->add('level', EntityType::class, [
                'class' => 'App:CourseLevel',
                'choice_label' => 'name'
                ])
            ->add('submit', SubmitType::class);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Course::class,
        ]);
    }
}


