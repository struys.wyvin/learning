<?php

namespace App\Controller;

use App\Entity\Advert;
use App\Form\AdvertType;
use App\Repository\AdvertRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdvertController extends AbstractController
{
    /**
     * @Route("/advert-admin", name="advertAdmin")
     * @IsGranted("ROLE_ADMIN")
     */
    public function advertAdmin(AdvertRepository $repository )
    {

        $adverts = $repository->findAll();

        return $this->render('admin/advert/advert.html.twig', [
            'adverts' => $adverts,
        ]);
    }

    /**
     * @Route("/add-adverts", name="addAdverts")
     * @IsGranted("ROLE_ADMIN")
     */
    public function addAdverts(Request $request)
    {
        $advert = new Advert();
        $form = $this->createForm(AdvertType::class, $advert);
        $form->handleRequest($request);
        if ($form->isSubmitted() &&$form->isValid()){
            $em = $this->getDoctrine()
                       ->getManager();
        $advert->setCreatedAt(new \DateTime('now'));
        if(empty($advert->getImageFile())) $advert->setImage('default.jpg');
        $em->persist($advert);
        $em->flush();
        $this->addFlash(
            'success',
            'Votre Actualité à bien été posté !'
        );
        return $this->redirectToRoute('advertAdmin');
        }
        return $this->render('admin/advert/addAdvert.html.twig', [
            'advert' => $advert,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit-adverts-{id}", name="editAdverts")
     * @IsGranted("ROLE_ADMIN")
     */
    public function editAdverts(Request $request, Advert $advert)
    {
        $form = $this->createForm(AdvertType::class, $advert);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()
                       ->getManager();
            $em->persist($advert);
            $em->flush();
            $this->addFlash(
                'success',
                'Votre actualité à bien été modifié'
            );
            return $this->redirectToRoute('advertAdmin');
            
        }
        return $this->render('admin/advert/editAdvert.html.twig', [
            'advert' => $advert,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete-advert-{id}", name="delAdvert")
     * @IsGranted("ROLE_ADMIN")
     */
    public function delAdvert($id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Advert::class);
        $advert = $repository->find($id);
        $em->remove($advert);
        $em->flush();
        $this->addFlash(
            'success',
            'Votre actualité à bien été supprimé'
        );
        return $this->redirectToRoute('advertAdmin');
    }

    /**
     * @Route("view-advert-{id}", name="viewAdvert")
     * @IsGranted("ROLE_ADMIN")
     */
   public function viewCourse(ObjectManager $em, AdvertRepository $repository, $id)  
   {
    $advert = $repository->find($id);
    $advert->getIsPublished() ? $advert->setIsPublished(0) : $advert->setIsPublished(1);
    return $this->redirectToRoute('advertAdmin');
   }
}
