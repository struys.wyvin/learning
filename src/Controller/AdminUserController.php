<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class AdminUserController extends AbstractController
{
    /**
     * @Route("/user-admin", name="userAdmin")
     * @IsGranted("ROLE_ADMIN")
     */
    public function userAdmin(UserRepository $repository)
    {
        $users = $repository->findAll();



        return $this->render('admin/user/user.html.twig', [
            'users' => $users
        ]);
    }

     /**
     * @Route("view-user-{id}", name="viewUser")
     * @IsGranted("ROLE_ADMIN")
     */
   public function viewUser(ObjectManager $em, UserRepository $repository, $id)  
   {
    $user = $repository->find($id);
    $user->getIsDisabled() ? $user->setIsDisabled(0) : $user->setIsDisabled(1);
    $em->flush();
    return $this->redirectToRoute('userAdmin');
   }

     /**
     * @Route("promote-user-{id}", name="promoteUser")
     * @IsGranted("ROLE_ADMIN")
     */
    public function promoteUser(ObjectManager $em, UserRepository $repository, $id)  
    {
     $user = $repository->find($id);
     $user->setRole(['ROLE_ADMIN']);
     $em->flush();
     return $this->redirectToRoute('userAdmin');
    }

      /**
     * @Route("demote-user-{id}", name="demoteUser")
     * @IsGranted("ROLE_ADMIN")
     */
    public function demoteUser(ObjectManager $em, UserRepository $repository, $id)  
    {
     $user = $repository->find($id);
     $user->setRole(['ROLE_USER']);
     $em->flush();
     return $this->redirectToRoute('userAdmin');
    }
}
