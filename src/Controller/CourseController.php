<?php

namespace App\Controller;

use App\Entity\Course;
use App\Entity\Comment;
use App\Form\CourseType;
use App\Form\CommentType;
use App\Entity\CourseCategory;
use App\Repository\CourseRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class CourseController
 */
class CourseController extends AbstractController
{
    /**
     * @Route("/courses", name="courses")
     */
    public function courses(CourseRepository $repository)
    {
        $courses = $repository->findAll();

        $categories = $this->getDoctrine()
                           ->getRepository(CourseCategory::class)
                           ->findAll();

        return $this->render('course/courses.html.twig', [
            'courses' => $courses,
            'categories' => $categories
        ]);
    }

    /**
     * @Route("courses-{slug}-{id}", name="onepost", requirements={"slug": "[a-z0-9\-]*"})
     */
    public function onePost($slug, $id, Request $request)
    {

        $courses = $this->getDoctrine()
                        ->getRepository(Course::class)
                        ->find($id);
            
        $comments = $this->getDoctrine()
                         ->getRepository(Course::class)
                         ->find($id)
                         ->getComments();            

        $commentary = new Comment();
        $form = $this->createForm(CommentType::class, $commentary);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()
                ->getManager();
            $commentary->setUser($this->getUser());
            $commentary->setCreatedAt(new \DateTime('now'));
            $courses->addComment($commentary);
            $em->persist($commentary);
            $em->flush();
            $this->addFlash(
                'success',
                'Votre commentaire a bien été ajouté'
            );
            return $this->redirectToRoute('onepost', ['id' => $id, 'slug' => $slug]);
        }

        return $this->render('course/details.html.twig', [
            'courses' => $courses,
            'comments' => $comments,
            'form' => $form->createView()
        ]);
    }
}
