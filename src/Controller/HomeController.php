<?php

namespace App\Controller;

use App\Entity\Advert;
use App\Entity\Course;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class HomeController
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home()
    {
        $courses = $this->getDoctrine()
                        ->getRepository(Course::class)
                        ->findAll();

        $adverts = $this->getDoctrine()
                       ->getRepository(Advert::class)
                       ->findAll();
        return $this->render('home/index.html.twig', [
            'courses' => $courses,
            'adverts' => $adverts
        ]);
    }

    /**
     * @Route("/advert", name="advert")
     */
    public function advert()
    {
        $adverts = $this->getDoctrine()
                        ->getRepository(Advert::class)
                        ->findAll();
        
        return $this->render('home/alladverts.html.twig',[
            'adverts' => $adverts
        ]);
    }

    /**
     * @Route("/advert-{id}", name="advertdetails")
     */
    public function advertdetails($id, Request $request)
    {
        $advert = $this->getDoctrine()
                        ->getRepository(Advert::class)
                        ->find($id);

        return $this->render("home/advert.html.twig", [
            'advert' => $advert
        ]);
    }
}
