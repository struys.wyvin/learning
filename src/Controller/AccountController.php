<?php

namespace App\Controller;

use DateTime;
use App\Entity\User;
use App\Entity\PasswordUpdate;
use App\Form\UpdateAccountType;
use App\Form\PasswordUpdateType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class AccountController extends AbstractController
{
    /**
     * @Route("/myaccount", name="myaccount")
     * @IsGranted("ROLE_USER")
     */
    public function myaccount()
    {
        return $this->render('account/account.html.twig');
    }

    /**
     * @Route("/updateaccount-{id}", name="updateaccount")
     * @IsGranted("ROLE_USER")
     */
    public function updateaccount(Request $request, User $user)
    {
        $form = $this->createForm(UpdateAccountType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()
                ->getManager();
            $user->setUpdatedAt(new \DateTime('now'));
            if(empty($user->getImageFile())) $user->setImage('default.jpg');
            $em->persist($user);
            $em->flush();
            $this->addFlash(
                'succes',
                'Vos données utilisateur ont bien été modifié'
            );
            return $this->redirectToRoute('myaccount');

        }
        return $this->render('account/update.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
}
