<?php

namespace App\Controller;

use App\Repository\InscriptionRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class AdminInscriptionController extends AbstractController
{
    /**
     * @Route("/admin-inscription", name="adminInscription")
     * @IsGranted("ROLE_ADMIN")
     */
    public function adminInsc(InscriptionRepository $repository)
    {
        $reserves =$repository->findAll();


        return $this->render('admin/Inscription/inscription.html.twig', [
            'reserves' => $reserves,
        ]);
    }
}
