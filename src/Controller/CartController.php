<?php

namespace App\Controller;

use App\Entity\Course;
use App\Entity\Inscription;
use App\Form\InscriptionType;
use App\Repository\CourseRepository;
use App\Repository\InscriptionRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CartController extends AbstractController
{
    /**
     * @Route("/reservationListUser", name="reservationListUser")
     * @IsGranted("ROLE_USER")
     */
    public function index(InscriptionRepository $repository)
    {
        {
            $reserves = $repository->findAll();
        }
    
        return $this->render('cart/index.html.twig', [
            'reserves' => $reserves
        ]);

    }

    /**
     * @Route("/add-reservation-{id}", name="addReserve")
     * @IsGranted("ROLE_USER")
     */
    public function addReserve($id, Request $request)
    {

        $courses = $this->getDoctrine()
                        ->getRepository(Course::class)
                        ->find($id);              
            
        $reserve = new Inscription();
        $form = $this->createForm(InscriptionType::class, $reserve);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()
                ->getManager();
            $reserve->setUser($this->getUser());
            $courses->addInscription($reserve);
            $reserve->setIsConfirmed(0);
            $em->persist($reserve);
            $em->flush();
            $this->addFlash(
                'success',
                'Vous êtes inscrits à ce cours'
             );
            return $this->redirectToRoute('reservationListUser');
        }
        return $this->render('cart/addReserve.html.twig', [
            'reserve' => $reserve,
            'courses' => $courses,
            'form' => $form->createView()
        ]);
       
    }

    /**
     * @Route("/reserve-delete-{id}", name="delReserve")
     * @IsGranted("ROLE_USER")
     */
    public function delReserve($id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Inscription::class);
        $reserve = $repository->find($id);
        $em->remove($reserve);
        $em->flush();
        $this->addFlash(
            'success',
            'Vous avez bien été désinscris du cours'
        );
        return $this->redirectToRoute('reservationListUser');
    }
}
