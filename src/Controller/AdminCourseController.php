<?php

namespace App\Controller;

use DateTime;
use App\Entity\Course;
use App\Form\CourseType;
use App\Entity\CourseCategory;
use App\Repository\CourseRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class AdminCourseController extends AbstractController
{
    /**
     *  @Route("/courses-admin", name="coursesAdmin")
     * @IsGranted("ROLE_ADMIN")
     */
    public function adminCourses(CourseRepository $repository)
    {
        $courses = $repository->findAll();

        
        $categories = $this->getDoctrine()
                           ->getRepository(CourseCategory::class)
                           ->findAll();

        return $this->render('admin/course/courses.html.twig', [
            'courses' => $courses,
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/add-courses", name="addCourses")
     * @IsGranted("ROLE_ADMIN")
     */
    public function addCourses(Request $request)
    {
        $course = new Course();
        $form = $this->createForm(CourseType::class, $course);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()
                ->getManager();
        $course->setUser($this->getUser());
        $course->setSlug($course->getName());
        $course->setCreatedAt(new \DateTime('now'));
        if(empty($course->getImageFile())) $course->setImage('default.jpg');
        $em->persist($course);
        $em->flush();
        $this->addFlash(
            'success',
            'Votre Cours à bien été ajouté'
        );
        return $this->redirectToRoute('coursesAdmin');
        }
        return $this->render('admin/course/addCourse.html.twig', [
            'course' => $course,
            'form' => $form->createView()
        ]);
    }

    /**
     *  @Route("edit-course-{id}", name="editCourse")
     * @IsGranted("ROLE_ADMIN")
     */
    public function editCourse(Request $request, Course $course)
    {
        $form = $this->createForm(CourseType::class, $course);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $em =$this->getDoctrine()
                ->getManager();
            $course->setUser($this->getUser());
            $course->setSlug($course->getName());
            $course->setUpdatedAt(new \DateTime('now'));
            $em->persist($course);
            $em->flush();
            $this->addFlash(
                'success',
                'Votre cours  à bien été modifié'
            );
            return $this->redirectToRoute('coursesAdmin');
        }
        return $this->render('admin/course/editCourse.html.twig', [
            'course' => $course,
            'form' => $form->createView()
        ]);
        
    }

    /**
     * @Route("delete-course-{id}", name="delCourse")
     * @IsGranted("ROLE_ADMIN")
     */
    public function delCourse($id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Course::class);
        $course = $repository->find($id);
        $em->remove($course);
        $em->flush();
        $this->addFlash(
            'succes',
            'Votre cours a bien été supprimé'
        );
        return $this->redirectToRoute('coursesAdmin');
    }

    /**
     * @Route("view-course-{id}", name="viewCourse")
     * @IsGranted("ROLE_ADMIN")
     */
   public function viewCourse(ObjectManager $em, CourseRepository $repository, $id)  
   {
    $course = $repository->find($id);
    $course->getIsPublished() ? $course->setIsPublished(0) : $course->setIsPublished(1);
    return $this->redirectToRoute('coursesAdmin');
   }
}