<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Repository\CommentRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminCommentController extends AbstractController
{
    /**
     * @Route("/comments-admin", name="commentsAdmin")
     */
    public function adminComments(CommentRepository $repository)
    {

        $comments = $repository->findAll();

        return $this->render('admin/comment/comment.html.twig', [
            'comments' => $comments
        ]);
    }

      /**
     * @Route("delete-comment-{id}", name="delComment")
     */
    public function delComment($id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Comment::class);
        $comment = $repository->find($id);
        $em->remove($comment);
        $em->flush();
        $this->addFlash(
            'succes',
            'Votre commentaire a bien été supprimé'
        );
        return $this->redirectToRoute('commentsAdmin');
    }

}
