<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class PasswordUpdate 
{
    /**
     * @Assert\EqualTo(propertyPath="Password", message="Veuillez entrer votre ancien mot de passe")
     */
    private $oldPassword;

    /**
     * @Assert\Length(
     *      min = 6,
     *      minMessage = "Le mot de passe doit contenir au minimum {{ limit }} caractères"
     * )
     */
    private $newPassword;

   /**
     * @Assert\EqualTo(propertyPath="newPassword", message="Veuillez entrer votre ancien mot de passe")
     */
    private $confirmPassword;

    /**
     * Get the value of oldPassword
     */ 
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * Set the value of oldPassword
     *
     * @return  self
     */ 
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }

    /**
     * Get the value of newPassword
     */ 
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * Set the value of newPassword
     *
     * @return  self
     */ 
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;

        return $this;
    }

    /**
     * Get the value of confirmPassword
     */ 
    public function getConfirmPassword()
    {
        return $this->confirmPassword;
    }

    /**
     * Set the value of confirmPassword
     *
     * @return  self
     */ 
    public function setConfirmPassword($confirmPassword)
    {
        $this->confirmPassword = $confirmPassword;

        return $this;
    }
}
