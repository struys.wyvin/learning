<?php

namespace App\Service;

use App\Entity\Contact;
use Twig\Environment;       // Mail en HTML -> Twig

class ContactService
{
    private $mailer;

    private $renderer;

    public function __construct(\Swift_Mailer $mailer, Environment $renderer) {
        $this->mailer = $mailer;
        $this->renderer = $renderer;
    }

    public function sendMail(Contact $contact)
    {
        $message = (new \Swift_Message())
            ->setFrom($contact->getEmail())
            ->setTo('contact@easylearning.be')
            ->setReplyTo($contact->getEmail())
            ->setSubject($contact->getSubject())
            ->setBody($this->renderer->render('contact/emailbasic.html.twig', [
                'contact' => $contact
            ]), 'text/html');
        $this->mailer->send($message);
        
    }
}