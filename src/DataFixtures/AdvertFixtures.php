<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Advert;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AdvertFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for($i = 1; $i <= 10; $i++){
        $advert = new Advert();
        $advert->setName($faker->words(2, true));
        $advert->setContent($faker->paragraph(2, true));
        $advert->setCreatedAt($faker->dateTimeBetween('-3 months', 'now'));
        $advert->setIsPublished($faker->boolean(90));
        $advert->setImage($i.'.jpg');
        $manager->persist($advert);
        }

        $manager->flush();
    }
}
