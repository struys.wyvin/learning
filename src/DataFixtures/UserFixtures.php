<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Comment;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $genres = ['male', 'female'];
        for($i = 1; $i <= 40; $i++) {
            $user = new User();
            $slugify = new Slugify();
            $genre = $faker->randomElement($genres);
            $user->setFirstName($faker->firstName($genre));
            $user->setLastName($faker->lastName);
            $user->setUserName($user->getFirstName().$user->getLastName());
            $user->setEmail($slugify->slugify($user->getFirstName() . $user->getLastName()) . '@gmail.com');
            $genre = $genre == 'male' ? 'm' : 'f';
            $user->setImage('0'.($i+10).$genre.'.jpg');
            $user->setPassword('password');
            $user->setPassword($this->encoder->encodePassword($user, $user->getPassword()));
            $user->setCreatedAt($faker->dateTimeBetween('-6 months', 'now'));
            $user->setUpdatedAt($faker->dateTimeBetween('now'));
            $user->setIsDisabled($faker->boolean(20));
            $user->setRole(['ROLE_USER']);
            $manager->persist($user);
        }
        $manager->flush();
    }
}
