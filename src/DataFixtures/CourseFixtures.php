<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Course;
use Cocur\Slugify\Slugify;
use App\Entity\CourseLevel;
use App\Entity\CourseCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CourseFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $slugify = new Slugify();
        $levels = $manager->getRepository(CourseLevel::class)->findAll();
        $categories = $manager->getRepository(CourseCategory::class)->findAll();
        for ($i = 1; $i <= 25; $i++) {
            $course = new Course();
            $course->setName($faker->sentence(4, true));
            $course->setSmallDescription($faker->paragraphs(1, true));
            $course->setFullDescription('<p>'. implode('</p><p>', $faker->paragraphs(5)) . '</p>');
            $course->setDuration($faker->numberBetween(1, 5));
            $course->setPrice($faker->numberBetween(120, 500));
            $course->setCreatedAt($faker->dateTimeThisYear('now'));
            $course->setIsPublished($faker->boolean(90));
            $course->setSlug($slugify->slugify($course->getName()));
            $course->setImage($i . '.jpg');
            $course->setCategory($categories[$faker->numberBetween(0, count($categories) -1)]);
            $course->setLevel($levels[$faker->numberBetween(0, count($levels) -1)]);
            $manager->persist($course);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CourseCategoryFixtures::class,
            CourseLevelFixtures::class
        ];
    }
}
