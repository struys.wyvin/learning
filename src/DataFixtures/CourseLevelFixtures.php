<?php

namespace App\DataFixtures;

use App\Entity\CourseLevel;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CourseLevelFixtures extends Fixture
{
    private $nameLvl = ['Bachelier', 'Master', 'Doctorat'];
    private $prerequis = ['CESS', 'CEB', 'CESI', 'CQ', 'CE1D'];

    public function load(ObjectManager $manager)
    {

        for ($i = 0; $i <= 5; $i++) {
            $courseLvl = new CourseLevel();
            $courseLvl->setName($this->nameLvl[array_rand($this->nameLvl)]);
            var_dump($this->nameLvl);
            $courseLvl->setPrerequisite($this->prerequis[array_rand($this->prerequis)]);
            $manager->persist($courseLvl);
        }
        $manager->flush();
    }
}
