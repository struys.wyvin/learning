<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\CourseCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Category;



class CourseCategoryFixtures extends Fixture
{
    private $categories = ['Informatique', 'Langue', 'Artisanat', 'Mode', 'Art'];

    public function load(ObjectManager $manager)
    {
        
        $faker = Factory::create('fr_FR');
        foreach($this->categories as $cat) {
            $category = new CourseCategory();
            $category->setName($cat);
            $category->setDescription($faker->paragraph(3, false));
            $manager->persist($category);
        }
        $manager->flush();
    }
}
