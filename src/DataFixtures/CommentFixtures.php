<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Course;
use App\Entity\Comment;
use App\DataFixtures\UserFixtures;
use App\DataFixtures\CourseFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $user = $manager->getRepository(User::class)->findAll();
        $course = $manager->getRepository(Course::class)->findAll();
        for($i = 1; $i <= 20; $i++){
            $comment = new Comment();
            $comment->setContent($faker->text($maxNbChars = 120));
            $comment->setCreatedAt($faker->dateTimeThisYear('now'));
            $comment->setRating($faker->numberBetween(0, 5));
            $comment->setUser($user[$faker->numberBetween(0, count($user) -1)]);
            $comment->setCourse($course[$faker->numberBetween(0, count($course) -1)]);
            $manager->persist($comment);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            CourseFixtures::class
        ];
    }
}
